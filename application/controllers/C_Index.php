<?php
class C_Index extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function checkSession(){
    	if ($this->session->userdata('logged_in')!=TRUE) {
    		redirect(base_url('login'));
        }else {
            
    	}
    }

	public function login()
	{
		$this->load->view('V_Index');
	}

	public function index()
	{
		$this->checkSession();
		$this->load->view('V_Dashboard');
	}
}